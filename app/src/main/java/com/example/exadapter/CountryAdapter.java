package com.example.exadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CountryAdapter extends BaseAdapter {
    Context c;
    ArrayList<Country> data;
    int resource;
    int image;
    String countryName_str,countryCapital_str;

    public CountryAdapter(Context c, ArrayList<Country> data, int resource) {
        this.c = c;
        this.data = data;
        this.resource = resource;
    }
    public void addItem(Country country){
        data.add(country);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            v= LayoutInflater.from(c).inflate(resource,null,false);
        }
        TextView countryName = v.findViewById(R.id.country_name);
        TextView countryCapital = v.findViewById(R.id.country_capital);
        ImageView countryImage = v.findViewById(R.id.country_img);
        Country country = (Country) getItem(position);
        image = country.getCountryImage();
        countryName_str = country.getCountryName();
        countryCapital_str = country.getCountryCapital();

        countryName.setText(countryName_str);
        countryCapital.setText(countryCapital_str);
        countryImage.setImageResource(image);
        return v;
    }
}
