package com.example.exadapter;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity {
    ListView li;
    Button addCountry;
    CountryAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        li = findViewById(R.id.Li01);
        addCountry = findViewById(R.id.add_country_btn);
        ArrayList<Country> names = new ArrayList<>();
         myAdapter = new CountryAdapter(this,names,R.layout.list_item);
        li.setAdapter(myAdapter);
        addCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),AddCountryActivity.class);
                startActivityForResult(intent,1);

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode==RESULT_OK){
            Country country = (Country) data.getSerializableExtra("data");
            myAdapter.addItem(country);
            myAdapter.notifyDataSetChanged();

        }
    }
}
