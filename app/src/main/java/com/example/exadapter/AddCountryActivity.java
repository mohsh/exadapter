package com.example.exadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddCountryActivity extends AppCompatActivity {
    EditText countryName,countryCapital;
    Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_country);
        countryName = findViewById(R.id.country_name_et);
        countryCapital = findViewById(R.id.country_capital_et);
        add = findViewById(R.id.add_btn);

        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = countryName.getText().toString();
                String capital = countryCapital.getText().toString();
                Country country = new Country(name,capital,R.drawable.palestine);
                Intent intent = new Intent(getBaseContext(),ListViewActivity.class);
                intent.putExtra("data",country);
                setResult(RESULT_OK,intent);
                finish();

            }
        });
    }
}
