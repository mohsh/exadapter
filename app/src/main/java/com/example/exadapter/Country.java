package com.example.exadapter;

import java.io.Serializable;

public class Country implements Serializable {
    String countryName,countryCapital;
    int countryImage;

    public Country(String countryName, String countryCapital, int countryImage) {
        this.countryName = countryName;
        this.countryCapital = countryCapital;
        this.countryImage = countryImage;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCapital() {
        return countryCapital;
    }

    public void setCountryCapital(String countryCapital) {
        this.countryCapital = countryCapital;
    }

    public int getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(int countryImage) {
        this.countryImage = countryImage;
    }
}
